# GITLAB CI
# Checks code quality, runs tests, and then uses Docker-in-Docker
# to build the Dockerfiles and upload them to GitLab's Container Registry.

stages:
    - test
    - build

######
# TEST
######

# Run Go unit tests.
go-unit-tests:
    stage: test
    image: golang:1.14-buster
    script:
        - cd backend
        - go test -race -v -cover ./...
    coverage: '/coverage: \d+.\d+% of statements/'
    allow_failure: true
    needs: []

# Run golangci-lint, a unified Go linter, code quality analyzer and
# style checker. See: https://github.com/golangci/golangci-lint
go-lint:
    stage: test
    image: golang:1.14-buster
    # Download golangci-lint binary from GitHub
    before_script:
        -  curl -sSfL https://raw.githubusercontent.com/golangci/golangci-lint/master/install.sh | sh -s -- -b /go/bin v1.27.0
    # Cache it so we don't have to redownload it every time the pipeline runs
    cache:
        paths:
            - /go/bin/golangci-lint
    script:
        - cd backend
        - golangci-lint run
    allow_failure: true
    needs: []

# Run Prettier, a multi-language (HTML, CSS, JS) style checker for the frontend.
# Prettier is not explicitly configured because it gets style rules from the
# .editorconfig file at the project root.
prettier:
    stage: test
    image: node:12-buster
    before_script:
        - cd frontend
        - yarn
    cache:
        paths:
            - ./frontend/node_modules
    script:
        - yarn run prettier --check './*'
    # Must respect code style! >:(
    allow_failure: false
    needs: []

#######
# BUILD
#######

build-backend:
    stage: build
    image: docker:19.03.12
    services:
        - docker:19.03.12-dind
    variables:
        IMAGE_TAG: $CI_REGISTRY_IMAGE:backend-$CI_COMMIT_REF_SLUG
    script:
        - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
        - docker build --cache-from $IMAGE_TAG -f build/prod.backend.Dockerfile -t $IMAGE_TAG backend
        - docker push $IMAGE_TAG
    needs: []

build-frontend:
    stage: build
    image: docker:19.03.12
    services:
        - docker:19.03.12-dind
    variables:
        IMAGE_TAG: $CI_REGISTRY_IMAGE:frontend-$CI_COMMIT_REF_SLUG
    script:
        - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
        - docker build --cache-from $IMAGE_TAG -f build/prod.frontend.Dockerfile -t $IMAGE_TAG frontend
        - docker push $IMAGE_TAG
    needs: []
