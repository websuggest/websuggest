# MAKEFILE
# All of the targets in this Makefile are wrappers around Docker
# and Docker-Compose commands. Thus this Makefile is not strictly
# necessary in any sense but it is useful for saving a few
# keystrokes in development, especially since there are multiple
# Docker-Compose configs and they live inside the build/ directory
# so it is necessary to manually specify which config to use.
# 
# BASIC USAGE:
# To control Docker-Compose in development:
#     make devel.<action> [svc=<service>]
#
# To control Docker-Compose in production:
#     make prod.<action> [svc=<service>]
#
# <action> can be either one of:
#     build - Build the container for the service, if applicable
#     up - Run the service
#     down - Kill the service
#     logs - Tail the logs of the service
#     restart - Restart the service
#     run - Run a command inside the container of the service
# <service> can be either one of:
#     frontend - Frontend application (Nuxt.js)
#     backend - Backend application (Golang)
#     postgres - PostgreSQL DBMS system
# If svc is omitted then the action is applied to all services.
#
# OTHER TARGETS:
#     make devel.setup
#         Set up the development environment.
#     make {devel|prod}.migrate
#         Migrate the database.
#     make devel.test
#         Run the tests.
# 	  make {devel|prod}.yarn
#         Install NPM dependencies from package.json.
#     make devel.yarn.{add|remove}
#         Add and remove NPM dependencies.
#     make devel.yarn.upgrade
#         Interactively upgrade NPM dependencies to a safe release.
#         (minor SemVer versions, usually works without further intervention)
#     make devel.yarn.full-upgrade
#         Interactively upgrade NPM dependencies to the latest release.
#         (major SemVer versions, expect API breakage)
#     make devel.makemigration name=<name>
#         Generate a blank new set of SQL migrations with the name
#         specified and the correct timestamp.
#     make lint.docker
#         Lint the Dockerfiles using hadolint.
#         (see https://github.com/hadolint/hadolint)

# Linting
lint.docker:
	docker run --rm -i -v ${PWD}/build/:/docker hadolint/hadolint \
		sh -c "hadolint /docker/*.Dockerfile"

# Development
docker_compose_devel = docker-compose -f build/devel.docker-compose.yml

devel.build:
	$(docker_compose_devel) build $(svc)
devel.up:
	$(docker_compose_devel) up -d $(svc)
devel.down:
	$(docker_compose_devel) down $(svc)
devel.logs:
	$(docker_compose_devel) logs --follow $(svc)
devel.restart:
	$(docker_compose_devel) restart $(svc)
devel.run:
	$(docker_compose_devel) run $(svc) $(cmd)
devel.makemigration:
	$(docker_compose_devel) run api-server migrate create -dir db/migrations -ext sql $(name)
devel.migrate:
	$(docker_compose_devel) run api-server \
		migrate \
		-database postgresql://postgres:password@postgres:5432/postgres?sslmode=disable \
		-path db/migrations up
devel.test:
	$(docker_compose_devel) run api-server go test -v ./...
devel.yarn:
	$(docker_compose_devel) run frontend yarn
devel.yarn.add:
	$(docker_compose_devel) run frontend yarn add $(pkg)
devel.yarn.remove:
	$(docker_compose_devel) run frontend yarn remove $(pkg)
devel.yarn.upgrade:
	$(docker_compose_devel) run frontend yarn upgrade-interactive
devel.yarn.full-upgrade:
	$(docker_compose_devel) run frontend yarn upgrade-interactive --latest
devel.yarn.run:
	$(docker_compose_devel) run frontend yarn run $(cmd)
devel.setup: devel.up devel.migrate devel.yarn devel.restart

# Production
docker_compose_prod = docker-compose -f build/prod.docker-compose.yml

prod.build:
	$(docker_compose_prod) build $(svc)
prod.up:
	$(docker_compose_prod) up -d $(svc)
prod.down:
	$(docker_compose_prod) down $(svc)
prod.logs:
	$(docker_compose_prod) logs --follow $(svc)
prod.restart:
	$(docker_compose_prod) restart $(svc)
prod.run:
	$(docker_compose_prod) run $(svc) $(cmd)
prod.migrate:
	$(docker_compose_prod) run api-server \
		migrate \
		-database postgresql://postgres:${DB_PASSWORD}@postgres:5432/postgres?sslmode=disable \
		-path /usr/share/websuggest/migrations up
prod.yarn:
	$(docker_compose_prod) run frontend yarn
