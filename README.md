
# WebSuggest

![GitLab CI status](https://gitlab.com/websuggest/websuggest/badges/master/pipeline.svg) ![GitLab CI test coverage](https://gitlab.com/websuggest/websuggest/badges/master/coverage.svg)

> WebSuggest is under development and highly unstable. You should wait until 1.0 before using it.

WebSuggest is an open-source advertising network that focuses on privacy, security and user-friendliness. It attempts to bring ethical design to the field of internet advertising.

Major features:
* Free software (AGPL3)
* Self-hosted, decentralized and federated
* Privacy-conscious - doesn't track visitors
* Secure - filters out malvertising, scams, and clickfraud
* Versatile - supports a wide variety of use cases
* Smart - uses intelligent algorithms to serve ads

See the [Roadmap](https://gitlab.com/websuggest/websuggest/-/issues/2) for more details.

## Development

On the backend we use [Go](https://golang.org/) with [PostgreSQL](https://www.postgresql.org/) via [GORM](https://gorm.io). We use [Chi](https://github.com/go-chi/chi) for routing.

On the frontend we use [Nuxt.js](https://nuxtjs.org/), [TypeScript](https://www.typescriptlang.org/) and [SASS](https://sass-lang.com/).

The directory structure is as follows:
- **backend/** - Backend code written in Go.
- **backend/db/** - PostgreSQL database configuration.
- **backend/db/models/** - GORM models.
- **backend/db/migrations/** - Raw SQL database migrations.
- **backend/cmd/** - Each subdirectory is a separate binary that manages one aspect of WebSuggest.
- **backend/cmd/websuggest-ad-server** - The server that serves advertisements.
- **backend/cmd/websuggest-api-server** - The main backend of WebSuggest. Provides an API for the web dashboard.
- **backend/cmd/websuggest-transactions** - The command-line tool that updates and executes payment transactions.
- **backend/pkg** - Supporting library code for the WebSuggest backend services.
- **frontend/** - Frontend code and assets. Follows the [Nuxt.js directory structure](https://nuxtjs.org/guide/directory-structure/).
- **build/** - Stuff related to deployment, DevOps, and CI/CD

### Getting Started with Docker

A Makefile is included for ease of development. If you have Docker, all you need to do to get started is clone the repository and run:
```bash
$ make devel.setup
```
This will use [Docker Compose](https://docs.docker.com/compose/) to build and run the project inside a set of Docker containers.

You can read the [Makefile](./Makefile) for further documentation.

### Getting Started without Docker

#### Backend

Dependencies:
* PostgreSQL 12
* Go 1.14

```bash
$ cd backend
$ go get -u github.com/cosmtrek/air
$ air
```

#### Frontend

Dependencies:
* Node.js 12
* Yarn 1.22

```bash
$ cd frontend
$ yarn
$ yarn run nuxt dev
```
