FROM golang:1.14-buster

# Air is a development tool for Go that recompiles and reloads
# the server when you change a source file.
RUN go get -u -v github.com/cosmtrek/air

# golang-migrate is a command-line tool that runs SQL migrations
# against a database. We download the binary as a Debian package
# rather than building it from source since this is easier.
# https://github.com/golang-migrate/migrate
RUN curl -s https://packagecloud.io/install/repositories/golang-migrate/migrate/script.deb.sh | bash
RUN apt-get install -y migrate=4.11.0
