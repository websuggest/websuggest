###################
# Build environment
###################

FROM golang:1.14-buster AS builder

WORKDIR /app
COPY . /app

# Set Go compilation options
ARG OS
ENV GOOS=$OS
ARG ARCH
ENV GOARCH=$ARCH
ENV CGO_ENABLED=0

# Compile & install all WebSuggest binaries to GOPATH
RUN go mod download
RUN go mod verify
RUN go install -v -ldflags="-w -s" ./...

# Compile go-migrate binary with Postgres support & install to GOPATH
RUN go get -tags 'postgres' -u github.com/golang-migrate/migrate/v4/cmd/migrate

#########################
# Minimal run environment
#########################

FROM alpine:3.12

COPY --from=builder /go/bin /usr/bin
RUN mkdir -p /usr/share/websuggest
COPY --from=builder /app/db/migrations /usr/share/websuggest/migrations
