###################
# Build environment
###################

FROM node:12-buster AS builder

WORKDIR /app
COPY . /app

# Download and compile everything
RUN yarn
RUN yarn run nuxt build
RUN yarn run nuxt export

#################
# Run environment
#################

FROM nginx:stable-alpine

COPY --from=builder /app/dist /usr/share/nginx/html
