/**
 * Nuxt.js is a framework built on top of Vue.js that
 * pre-renders Vue applications to improve SEO and performance
 * while retaining the flexibility and ease of development of
 * client-side rendering. It also provides a set of sane, opinionated
 * defaults for building Vue applications.
 */

export default {
    // Use TypeScript
    buildModules: ["@nuxt/typescript-build"],
    // Build a static site instead of an SSR Node.js server
    // (this is cheaper and more performant)
    target: "static"
};
