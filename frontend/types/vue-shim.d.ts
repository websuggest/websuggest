// Shim to enable TypeScript support for Vue.js
// single-file components.

declare module "*.vue" {
    import Vue from "vue";
    export default Vue;
}
